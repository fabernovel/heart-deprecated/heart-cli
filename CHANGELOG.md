## [1.2.1] - 2019-07-15
### Changed
- Heart wiki url from the README now properly redirect to the repository used before _Heart_ version 3

## [1.2.0] - 2019-06-26

### Added
- Show analysis result
- Display an error for invalid command name

### Changed
- Improve README

### Fixed
- Remove the build directory lib/
- NPM publication

## [1.1.0] - 2019-06-05
### Added
- The module is able to handle Storage modules

## [1.0.0] - 2019-04-23
### Added
- First release (YaY!)
